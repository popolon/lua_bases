* français [README.fr.md](README.fr.md)
* 中文 [README.zh.md](README.zh.md)

Officials repositories
* https://framagit.org/popolon/lua_bases
* https://gitee.com/Popolon/lua_bases

# Basic principles of my class implementation

```lua
m=math sqr=m.sqrt strf=string.format
V={__call=function(s,X,Y)local s={}setmetatable(s,V)s.x=X s.y=Y return s end,
__add=function(s,v)return V(s.x+v.x,s.y+v.y)end,
__tostring=function(s)return strf(":x=%.2fy=%.2f",s.x,s.y)end,
l=function(s)return sqr(s.x^2+s.y^2)end
}setmetatable(V,V)V.__index=V
```

A working example : https://tic80.com/play?cart=1852

# What functions does
Warning, there are no test, the goal is to have a fast calculation for games/demos/interactive content. Please, use with care, and double test the results

* V() constructor, warning: vec:v() create new instance for duplication else only pointer will be copied
* __add/__sub +/- operators, in combination with vector or scalar
* __unm uniq - operator (negate a vector)
* __mul × operator, in combination with scalar (homothety) or a vector (2d scaling deformation)
* __div ÷ operator, in combination with scalar
* __idiv // operator, in combination with scalar
* __len # operator, length of the vector
* __tostring,__concat used for string conversion, mainly for debug purpose
* v() create a new copy of the vector
* A() angle 
* d() dot product
* U() Normalized vector (french Unitaire)
* N() perpendicular vector (french Normale)
* R() rotation by an angle (in radian, S.I.)
* CP() cartesian to polar (in polar mode, x=length, y=angle to avoid class duplication)
* Paa/Paan Add an angle to a polar vector (in a new, returned vector if -n suffix)
* Pal/Paln Add length to a polar vector (in a new returned vector if -n suffix)
* PC() polar to cartesian
* PCu() pol2car, return Normalized vector (fr. unitaire) optimized function
* PCn() pol2car, return perpendicular normalized vector (fr. normale) optimized function
* GT()/GE()/EQ() Test if distance between two vectors is greater than, greater or equal to, equal to a scalar, optimized functions

Polar coordoninates are always in radian (S.I. unit, so easier to manipulate/faster/normalized degrees should be only used in human interfaces never in computations.

* Some usual Pi and golden ratio constantes are defined for faster computation
* You can find some example of usage in [my TIC-80 carts](https://tic80.com/dev?id=4645)

# The class itself following by some usage examples
```lua
m=math sqr=m.sqrt flr=m.floor cs=m.cos sn=m.sin at=m.atan
pi=m.pi pi2=pi*2 p12=pi/2 p14=pi/4 gd=(1+sqr(5))/2 g1d=1/gd
rnd=m.random strf=string.format
-- cartesian vector
V={__call=function(s,X,Y)local s={}setmetatable(s,V)s.x=X s.y=Y return s end,
__add=function(s,v)if type(v)=='number' then return V(s.x+v,s.y+v)else return V(s.x+v.x,s.y+v.y)end end,
__sub=function(s,v)if type(v)=='number' then return V(s.x-v,s.y-v)else return V(s.x-v.x,s.y-v.y)end end,
__unm=function(s)return V(-s.x,-s.y)end,
__mul=function(s,v)if type(v)=='number' then return V(s.x*v,s.y*v)else return V(s.x*v.x,s.y*v.y)end end,
__div=function(s,v)return V(s.x/v,s.y/v)end,
__idiv=function(s,v)return V(flr(s.x/v),flr(s.y/v))end,
__len=function(s)return sqr(s.x^2+s.y^2)end,
__tostring=function(s)return strfmt("x=%.2f y=%.2f",s.x,s.y)end,
__concat=function(s,c)return tostring(s)..tostring(c) end,
v=function(s)return V(s.x,s.y)end,
A=function(s)return at(s.y,s.x)end,
d=function(s,v)return s.x*v.x+s.y*v.y)end,
U=function(s)local l=#s if l~=0then return s*(1/l)else return V(0,0)end end,
N=function(s)local u=s:U()return V(u.y,-u.x)end,
R=function(s,p)local c,n=cs(p),sn(p)return V(s.x*c-s.y*n,s.x*n+s.y*c)end,
CP=function(s)return V(#s,at(s.y,s.x))end,
Paan=function(s,a)return V(s.x,s.y+a)end,
Paa=function(s,a)s.y=s.y+a end,
Paln=function(s,l)return V(s.x+l,s.y)end,
Pal=function(s,l)s.x=s.x+l end,
PCu=function(s)return V(cs(s.y),sn(s.y))end,
PCn=function(s)return V(sn(s.y),-cs(s.y))end,
PC=function(s)return V(s.x*cs(s.y),s.x*sn(s.y) )end
GT=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2>d*d end,
GE=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2>=d*d end,
EQ=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2==d*d end
}setmetatable(V,V)V.__index=V

-- Polar vector
--P={__call=function(s,L,T)local s={} setmetatable(s,P)s.l=L s.l=T return s end,
--PC=function(s)return V(s.l*cs(s.t),s.l*sn(s.t))end
--}setmetatable(P,P)P.__index=P

trace "---- muladd ---"
v=V(10,5) trace("v:"..v.."toto")
v2=v+5 trace("v2:"..v2)
v3=v-2-v2 trace("v3:"..v3)
v4=v3*2 trace("v4:"..v4)
v5=v3*v4 trace("v5:"..v5)
trace("V5:L():"..#v5)
trace("v3:D(v4):"..v3:D(v4))
trace("v3//5:"..v3//5)
trace("v:N():"..v:N())

trace("-- test longueurs")
v=V(-15,0)
v2=V(5,3)
V3=v-v2
trace("v:"..v)
trace("v2:"..v2)
trace("v:CP()"..v:CP())
trace("v:"..v.." l="..#v)
trace("v:U():"..v:U().." l="..#v:U())
trace("v:U():CP():"..v:U():CP().." l="..v:U():CP().x)
trace("-v:U():"..-v:U().." l="..-#v:U())
trace("Is v:GT(v2,5)="..strf("%.2f",#(V3))..">5:"..tostring(v:GT(v2,5)))
trace("Is v:GE(v2,25):"..tostring(v:GE(v2,25)))
function TIC()end
```

# Resources I used to create it

## Generic class with class name as constructor
```lua
tbl = {  __call = function(table, val1, val2)
 return "Hello, from functor: " .. (val1 + val2)
end
} setmetatable(tbl, tbl)tbl.__index=tbl

message = tbl(2, 3); -- Calling the table like a function!
print ("message: " .. message)
```

* [_call in Lua official manual](https://www.lua.org/manual/5.4/manual.html#2.4)
* [_call in O'Reilly, Lua Quick Start](https://www.oreilly.com/library/view/lua-quick-start/9781789343229/532509e7-fdde-4fb7-82a9-5916fbbc1c8d.xhtml) (about functor)

* __call usage on Stackexchange: [Creating 2D Vectors 107237](https://codereview.stackexchange.com/a/107237), [Creating 2d vectors 83825](https://codereview.stackexchange.com/questions/83825/creating-2d-vectors)

## testing if type is scalar
https://gist.github.com/johannesgijsbers/880372fc8800e5d5f3e4

## More documentation about metatables/class tests
https://stackoverflow.com/questions/1092832/how-to-create-a-class-subclass-and-properties-in-lua

## make table callable
https://riptutorial.com/lua/example/8060/make-tables-callable

Metatable Events (overloadable operators in C++)
* http://lua-users.org/wiki/MetatableEvents
* http://www.lua.org/manual/5.1/manual.html#2.8
* http://www.lua.org/manual/5.4/manual.html#2.4

## Passing args to a subfunction
```lua
setmetatable(Vector, { __call = function(_, ...) return Vector.new(...) end })
```

* To be seen later: http://lua-users.org/wiki/SimpleLuaClasses
* bout metatables in Lua: https://luaworkgroup.github.io/tutorials/metatables/metatables.html
