m=math sqr=m.sqrt flr=m.floor cs=m.cos sn=m.sin at=m.atan
pi=m.pi pi2=pi*2 p12=pi/2 p14=pi/4 gd=(1+sqr(5))/2 g1d=1/gd
rnd=m.random strf=string.format

-- cartesian vector
V={__call=function(s,X,Y)local s={}setmetatable(s,V)s.x=X s.y=Y return s end,
__add=function(s,v)if type(v)=='number' then return V(s.x+v,s.y+v)else return V(s.x+v.x,s.y+v.y)end end,
__sub=function(s,v)if type(v)=='number' then return V(s.x-v,s.y-v)else return V(s.x-v.x,s.y-v.y)end end,
__unm=function(s)return V(-s.x,-s.y)end,
__mul=function(s,v)if type(v)=='number' then return V(s.x*v,s.y*v)else return V(s.x*v.x,s.y*v.y)end end,
__div=function(s,v)return V(s.x/v,s.y/v)end,
__idiv=function(s,v)return V(flr(s.x/v),flr(s.y/v))end,
__len=function(s)return sqr(s.x^2+s.y^2)end,
__tostring=function(s)return strf("x=%.2f y=%.2f",s.x,s.y)end,
__concat=function(s,c)return tostring(s)..tostring(c)end,
v=function(s)return V(s.x,s.y)end,
A=function(s)return at(s.y,s.x)end,
d=function(s,v)return s.x*v.x+s.y*v.y end,
D=function(s,v)return #(v-s)end,
U=function(s)local l=#s if l~=0 then return s*(1/l)else return V(0,0)end end,
N=function(s)local u=s:U()return V(u.y,-u.x)end,
R=function(s,p)local c,n=cs(p),sn(p)return V(s.x*c-s.y*n,s.x*n+s.y*c)end,
CP=function(s)return V(#s,at(s.y,s.x))end,
Paan=function(s,a)return V(s.x,s.y+a)end,
Paa=function(s,a)s.y=s.y+a end,
Paln=function(s,l)return V(s.x+l,s.y)end,
Pal=function(s,l)s.x=s.x+l end,
PCu=function(s)return V(cs(s.y),sn(s.y))end,
PCn=function(s)return V(sn(s.y),-cs(s.y))end,
PC=function(s)return V(s.x*cs(s.y),s.x*sn(s.y) )end,
GT=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2>d*d end,
GE=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2>=d*d end,
EQ=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2==d*d end
}setmetatable(V,V)V.__index=V

-- Polar vector
--P={__call=function(s,L,T)local s={} setmetatable(s,P)s.l=L s.l=T return s end,
--PC=function(s)return V(s.l*cs(s.t),s.l*sn(s.t))end
--}setmetatable(P,P)P.__index=P

trace "---- muladd ---"
v=V(10,5) trace("v:"..v.."toto")
v2=v+5 trace("v2:"..v2)
v3=v-2-v2 trace("v3:"..v3)
v4=v3*2 trace("v4:"..v4)
v5=v3*v4 trace("v5:"..v5)
trace("V5:L():"..#v5)
trace("v3:D(v4):"..v3:D(v4))
trace("v3//5:"..v3//5)
trace("v:N():"..v:N())

trace("-- test longueurs")
v=V(-15,0)
v2=V(5,3)
trace("v:"..v)
trace("v2:"..v2)
trace("v:CP()"..v:CP())
trace("v:"..v.." l="..#v)
trace("v:U():"..v:U().." l="..#v:U())
trace("v:U():CP():"..v:U():CP().." l="..v:U():CP().x)
trace("-v:U():"..-v:U().." l="..-#v:U())
trace("Is v:GT(v2,5)="..strf("%.2f",#(V3))..">5:"..tostring(v:GT(v2,5)))
trace("Is v:GE(v2,25):"..tostring(v:GE(v2,25)))
function TIC()end
