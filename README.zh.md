* français [README.fr.md](README.fr.md)
* english [README.en.md](README.en.md)

软件存储库网站

* https://framagit.org/popolon/lua_bases
* https://gitee.com/Popolon/lua_bases

# 我实现一个类的基本原则
```lua
m=math sqr=m.sqrt strf=string.format
V={__call=function(s,X,Y)local s={}setmetatable(s,V)s.x=X s.y=Y return s end,
__add=function(s,v)return V(s.x+v.x,s.y+v.y)end,
__tostring=function(s)return strf(":x=%.2fy=%.2f",s.x,s.y)end,
l=function(s)return sqr(s.x^2+s.y^2)end
}setmetatable(V,V)V.__index=V
```

实例: https://tic80.com/play?cart=1852

# 详细描述
要小心，这个类不做任何测试，它的目的是为游戏、演示、互动应用做快速计算。考虑在使用过程中对结果做几种类型的测试。

* V() 构造器，要小心： vec:v()创建一个新的重复实例，否则将只复制指针。
* __add/__sub +/- 算术运算符, 根根标量或向量
* __unm uniq - 算术运算符, 否定向量
* __mul × 算术运算符，根标量(位似变换）或向量（二维形变）
* __div ÷ 算术运算符，根标量
* __idiv // 算术运算符，根标量
* __len # 算术运算符，向量长度
* __tostring,__concat 转换为一个字符串，主要用于调试
* v() 创建一个新的向量副本
* A() 角度
* d() 点积
* U() 单位向量（法文Unitaire）
* N() 法线（法文Normale）
* R() 旋转（弧度）
* CP() 笛卡尔坐标转为极坐标（！！极坐标:x=长度, y=角度）
* Paa/Paan 把极坐标添加角度（n=新向量）
* Pal/Paln 加把极坐标添加A长度（n=新向量）
* PC() 极坐标系转为笛卡尔坐标
* PCu() 极坐标系转为笛卡尔坐标，单位向量（法文Unitaire）优化的函数
* PCn() 极坐标系转为笛卡尔坐标，法线（法文Normale）优化的函数
* GT()/GE()/EQ() 测试2个向量之间的距离是否大于，大于或等于，或等于一个标量，优化的函数

极坐标总是以弧度为单位（国际单位制）

* 一些常用的π（pi）或黄金比例（gd）的常数。
* You can find some example of usage in [my TIC-80 carts](https://tic80.com/dev?id=4645)

# 完整版
```lua
m=math sqr=m.sqrt flr=m.floor cs=m.cos sn=m.sin at=m.atan
pi=m.pi pi2=pi*2 p12=pi/2 p14=pi/4 gd=(1+sqr(5))/2 g1d=1/gd
rnd=m.random strf=string.format
-- cartesian vector
V={__call=function(s,X,Y)local s={}setmetatable(s,V)s.x=X s.y=Y return s end,
__add=function(s,v)if type(v)=='number' then return V(s.x+v,s.y+v)else return V(s.x+v.x,s.y+v.y)end end,
__sub=function(s,v)if type(v)=='number' then return V(s.x-v,s.y-v)else return V(s.x-v.x,s.y-v.y)end end,
__unm=function(s)return V(-s.x,-s.y)end,
__mul=function(s,v)if type(v)=='number' then return V(s.x*v,s.y*v)else return V(s.x*v.x,s.y*v.y)end end,
__div=function(s,v)return V(s.x/v,s.y/v)end,
__idiv=function(s,v)return V(flr(s.x/v),flr(s.y/v))end,
__len=function(s)return sqr(s.x^2+s.y^2)end,
__tostring=function(s)return strfmt("x=%.2f y=%.2f",s.x,s.y)end,
__concat=function(s,c)return tostring(s)..tostring(c) end,
v=function(s)return V(s.x,s.y)end,
A=function(s)return at(s.y,s.x)end,
d=function(s,v)return s.x*v.x+s.y*v.y)end,
U=function(s)local l=#s if l~=0then return s*(1/l)else return V(0,0)end end,
N=function(s)local u=s:U()return V(u.y,-u.x)end,
R=function(s,p)local c,n=cs(p),sn(p)return V(s.x*c-s.y*n,s.x*n+s.y*c)end,
CP=function(s)return V(#s,at(s.y,s.x))end,
Paan=function(s,a)return V(s.x,s.y+a)end,
Paa=function(s,a)s.y=s.y+a end,
Paln=function(s,l)return V(s.x+l,s.y)end,
Pal=function(s,l)s.x=s.x+l end,
PCu=function(s)return V(cs(s.y),sn(s.y))end,
PCn=function(s)return V(sn(s.y),-cs(s.y))end,
PC=function(s)return V(s.x*cs(s.y),s.x*sn(s.y) )end
GT=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2>d*d end,
GE=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2>=d*d end,
EQ=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2==d*d end
}setmetatable(V,V)V.__index=V

-- Polar vector
--P={__call=function(s,L,T)local s={} setmetatable(s,P)s.l=L s.l=T return s end,
--PC=function(s)return V(s.l*cs(s.t),s.l*sn(s.t))end
--}setmetatable(P,P)P.__index=P

trace "---- muladd ---"
v=V(10,5) trace("v:"..v.."toto")
v2=v+5 trace("v2:"..v2)
v3=v-2-v2 trace("v3:"..v3)
v4=v3*2 trace("v4:"..v4)
v5=v3*v4 trace("v5:"..v5)
trace("V5:L():"..#v5)
trace("v3:D(v4):"..v3:D(v4))
trace("v3//5:"..v3//5)
trace("v:N():"..v:N())

trace("-- test longueurs")
v=V(-15,0)
v2=V(5,3)
V3=v-v2
trace("v:"..v)
trace("v2:"..v2)
trace("v:CP()"..v:CP())
trace("v:"..v.." l="..#v)
trace("v:U():"..v:U().." l="..#v:U())
trace("v:U():CP():"..v:U():CP().." l="..v:U():CP().x)
trace("-v:U():"..-v:U().." l="..-#v:U())
trace("is v:GT(v2,5)="..strf("%.2f",#(V3))..">5:"..tostring(v:GT(v2,5)))
trace("is v:GE(v2,25):"..tostring(v:GT(v2,25)))
function TIC()end
```

# 我用来创建它的资源

## Generic class with name class as constructor
```lua
tbl = {  __call = function(table, val1, val2)
 return "Hello, from functor: " .. (val1 + val2)
end
} setmetatable(tbl, tbl)tbl.__index=tbl

message = tbl(2, 3); -- Calling the table like a function!
print ("message: " .. message)
```

* [Lua 官方手册中的 _call](https://www.lua.org/manual/5.4/manual.html#2.4)
* [O'Reilly, Lua Quick Start中的 _call](https://www.oreilly.com/library/view/lua-quick-start/9781789343229/532509e7-fdde-4fb7-82a9-5916fbbc1c8d.xhtml) (about functor)

* Stackexchange 上的__调用情况：[Creating 2D Vectors 107237](https://codereview.stackexchange.com/a/107237), [Creating 2d vectors 83825](https://codereview.stackexchange.com/questions/83825/creating-2d-vectors)

## 测试类型是否为标量
https://gist.github.com/johannesgijsbers/880372fc8800e5d5f3e4

## 有关元表/类测试的更多文档
https://stackoverflow.com/questions/1092832/how-to-create-a-class-subclass-and-properties-in-lua

## 使表格可调用
https://riptutorial.com/lua/example/8060/make-tables-callable

Metatable Events （C++中的可重载操作符）
* http://lua-users.org/wiki/MetatableEvents
* http://www.lua.org/manual/5.1/manual.html#2.8
* http://www.lua.org/manual/5.4/manual.html#2.4

## 将参数传递给子函数
```lua
setmetatable(Vector, { __call = function(_, ...) return Vector.new(...) end })
```

* 待会儿见：http://lua-users.org/wiki/SimpleLuaClasses
* 关于metatables：https://luaworkgroup.github.io/tutorials/metatables/metatables.html
