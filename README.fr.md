* english [README.en.md](README.en.md)
* 中文 [README.zh.md](README.zh.md)

Dépôts officiels:
* https://framagit.org/popolon/lua_bases
* https://gitee.com/Popolon/lua_bases

# Principe de base de mon implémentation d'une classe

```lua
m=math sqr=m.sqrt strf=string.format
V={__call=function(s,X,Y)local s={}setmetatable(s,V)s.x=X s.y=Y return s end,
__add=function(s,v)return V(s.x+v.x,s.y+v.y)end,
__tostring=function(s)return strf(":x=%.2fy=%.2f",s.x,s.y)end,
l=function(s)return sqr(s.x^2+s.y^2)end
}setmetatable(V,V)V.__index=V
```

Un exemple pratique en action : https://tic80.com/play?cart=1852

## Description détaillée
Attention cette classe ne fait aucun test, elle a pour but de faire des calculs rapides dans le cadre de jeux, demos, applications interactives de loisir. Pensez à faire plusieurs types de tests sur les résultats lors d'une utilisation.

* V() constructeur, attention : vec:v() crée une nouvelle instance pour la duplicatiobn, sinon, seul le pointeur sera copié.
* __add/__sub opérateurs +/-, en combinaison avec un autre vecteur ou un scalaire
* __unm uniq opérateur -, version négative du vecteur
* __mul opérateur × en cominaison avec un autre vecteur (déformation scalaire 2d) ou un scalaire (homothétie)
* __div opérateurs ÷ (en combinaison avec un scalaire)
* __idiv  opérateur // (en combinaison avec un scalaire)
* __len opérateur # longueur d'un vecteur
* __tostring,__concat utilisé pour la conversion vers des châines de caractère, principalement pour le débogue.
* v() Crée une nouvelle copie du vecteur
* A() Angle 
* d() Produit scalaire
* U() Vecteur unitaire
* N() Normale
* R() Rotation retourne l'angle (en radians) 
* CP() Cartesian vers Polaire (En mode polaire, x=longueur, y=angle pour éviter une duplication des classes)
* Paa/Paan Incrémante l'angle a un vecteur polaire (dans un nouveau vecteur pour la fonction avec le suffixe -n)
* Pal/Paln Incrémente la longueur du vecteur polaire (dans un nouveau vecteur pour la fonction avec le suffixe -n)
* PC() Polair vers Cartésien
* PCu() PC retournant un vecteur unitaire, fonction optimisée
* PCn() PC retournant la normale, fonction optimisée
* GT()/GE()/EQ() Teste si la distance entre 2 vecteurs est plus grand, plus grand ou égal, ou égal à un scalaire, fonctions optimisées

Les coordonnées polaires sont toujours en radian (unité S.I. (système international), donc plus facile/rapide/et normalisé pour les manipulation. Les degrés ne devraient être utilisé que dans les interfaces hommes machines.

* Quelques constantes relatives à Pi et au nombre d'or sont ici prédéfinies pour des calculs plus rapides, leur développement de Taylor est gourmand en calcul.

```lua
m=math sqr=m.sqrt flr=m.floor cs=m.cos sn=m.sin at=m.atan
pi=m.pi pi2=pi*2 p12=pi/2 p14=pi/4 gd=(1+sqr(5))/2 g1d=1/gd
rnd=m.random strf=string.format
-- cartesian vector
V={__call=function(s,X,Y)local s={}setmetatable(s,V)s.x=X s.y=Y return s end,
__add=function(s,v)if type(v)=='number' then return V(s.x+v,s.y+v)else return V(s.x+v.x,s.y+v.y)end end,
__sub=function(s,v)if type(v)=='number' then return V(s.x-v,s.y-v)else return V(s.x-v.x,s.y-v.y)end end,
__unm=function(s)return V(-s.x,-s.y)end,
__mul=function(s,v)if type(v)=='number' then return V(s.x*v,s.y*v)else return V(s.x*v.x,s.y*v.y)end end,
__div=function(s,v)return V(s.x/v,s.y/v)end,
__idiv=function(s,v)return V(flr(s.x/v),flr(s.y/v))end,
__len=function(s)return sqr(s.x^2+s.y^2)end,
__tostring=function(s)return strfmt("x=%.2f y=%.2f",s.x,s.y)end,
__concat=function(s,c)return tostring(s)..tostring(c) end,
v=function(s)return V(s.x,s.y)end,
A=function(s)return at(s.y,s.x)end,
d=function(s,v)return s.x*v.x+s.y*v.y)end,
U=function(s)local l=#s if l~=0then return s*(1/l)else return V(0,0)end end,
N=function(s)local u=s:U()return V(u.y,-u.x)end,
R=function(s,p)local c,n=cs(p),sn(p)return V(s.x*c-s.y*n,s.x*n+s.y*c)end,
CP=function(s)return V(#s,at(s.y,s.x))end,
Paan=function(s,a)return V(s.x,s.y+a)end,
Paa=function(s,a)s.y=s.y+a end,
Paln=function(s,l)return V(s.x+l,s.y)end,
Pal=function(s,l)s.x=s.x+l end,
PCu=function(s)return V(cs(s.y),sn(s.y))end,
PCn=function(s)return V(sn(s.y),-cs(s.y))end,
PC=function(s)return V(s.x*cs(s.y),s.x*sn(s.y) )end
GT=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2>d*d end,
GE=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2>=d*d end,
EQ=function(s,v,d)return (s.x-v.x)^2+(s.y-v.y)^2==d*d end
}setmetatable(V,V)V.__index=V

-- Polar vector
--P={__call=function(s,L,T)local s={} setmetatable(s,P)s.l=L s.l=T return s end,
--PC=function(s)return V(s.l*cs(s.t),s.l*sn(s.t))end
--}setmetatable(P,P)P.__index=P

trace "---- muladd ---"
v=V(10,5) trace("v:"..v.."toto")
v2=v+5 trace("v2:"..v2)
v3=v-2-v2 trace("v3:"..v3)
v4=v3*2 trace("v4:"..v4)
v5=v3*v4 trace("v5:"..v5)
trace("V5:L():"..#v5)
trace("v3:D(v4):"..v3:D(v4))
trace("v3//5:"..v3//5)
trace("v:N():"..v:N())

trace("-- test longueurs")
v=V(-15,0)
v2=V(5,3)
V3=v-v2
trace("v:"..v)
trace("v2:"..v2)
trace("v:CP()"..v:CP())
trace("v:"..v.." l="..#v)
trace("v:U():"..v:U().." l="..#v:U())
trace("v:U():CP():"..v:U():CP().." l="..v:U():CP().x)
trace("-v:U():"..-v:U().." l="..-#v:U())
trace("is v:GT(v2,5)="..strf("%.2f",#(V3))..">5:"..tostring(v:GT(v2,5)))
trace("is v:GE(v2,25):"..tostring(v:GT(v2,25)))
function TIC()end
```

# Les ressources que j'ai utilisé pour créé cette classe

## Classe générique avec nom de classe comme constructeur
```lua
tbl = {  __call = function(table, val1, val2)
 return "Hello, from functor: " .. (val1 + val2)
end
} setmetatable(tbl, tbl)tbl.__index=tbl

message = tbl(2, 3); -- Calling the table like a function!
print ("message: " .. message)
```

* [_call dans le manuel officie](https://www.lua.org/manual/5.4/manual.html#2.4)
* [_call sur O'Reilly, Lua Quick Start](https://www.oreilly.com/library/view/lua-quick-start/9781789343229/532509e7-fdde-4fb7-82a9-5916fbbc1c8d.xhtml) (notion de functor)

* Utilisation de __call sur Stackexchange: [Creating 2D Vectors 107237](https://codereview.stackexchange.com/a/107237), [Creating 2d vectors 83825](https://codereview.stackexchange.com/questions/83825/creating-2d-vectors)

## Test si type scalaire
https://gist.github.com/johannesgijsbers/880372fc8800e5d5f3e4

## Plus sur les metatables/test de classes
https://stackoverflow.com/questions/1092832/how-to-create-a-class-subclass-and-properties-in-lua


## Rend la table appelable
https://riptutorial.com/lua/example/8060/make-tables-callable

Evenements des metatables (opérateurs surchargeables en C++)
* http://lua-users.org/wiki/MetatableEvents
* http://www.lua.org/manual/5.1/manual.html#2.8
* http://www.lua.org/manual/5.4/manual.html#2.4

## Passage des arguments à une sous-fonction:
```lua
setmetatable(Vector, { __call = function(_, ...) return Vector.new(...) end })
```

* À voir plus tard: http://lua-users.org/wiki/SimpleLuaClasses
* À propos des metatables dans Lua : https://luaworkgroup.github.io/tutorials/metatables/metatables.html
